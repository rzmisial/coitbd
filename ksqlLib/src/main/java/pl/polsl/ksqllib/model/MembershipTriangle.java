/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTriangle;
import pl.polsl.ksqllib.fuzzyValue.IFuzzyValue;

/**
 *affi
 * @author Michal
 */
@UdfDescription(name = "membershipTriangle", description = "Fuzzifies a value and returns its membership to the fuzzifying triangle.")
public class MembershipTriangle {
    
    @Udf(description = "Find affiliation for a DOUBLE value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final double v,
            @UdfParameter(value="a") final double a,
            @UdfParameter(value="b") final double b,
            @UdfParameter(value="c") final double c)  {

        IFuzzyValue val = new FuzzyValueTriangle(a, b, c);
        return val.getLambda(v);

    }
    
    @Udf(description = "Find membership for a INT value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final int v,
            @UdfParameter(value="a") final int a,
            @UdfParameter(value="b") final int b,
            @UdfParameter(value="c") final int c) {

        return membershipTriangle((double) v, a, b, c);

    }
    
    @Udf(description = "Find membership for a BIGINT value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final long v,
            @UdfParameter(value="a") final long a,
            @UdfParameter(value="b") final long b,
            @UdfParameter(value="c") final long c) {

        return membershipTriangle((double) v, a, b, c);

    }
    
    @Udf(description = "Find affiliation for a DOUBLE value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final double v,
            @UdfParameter(value="centre") final double centre,
            @UdfParameter(value="armWidth") final double armWidth) {

        IFuzzyValue val = new FuzzyValueTriangle(centre, armWidth);
        return val.getLambda(v);

    }
    
    @Udf(description = "Find membership for a INT value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final int v,
            @UdfParameter(value="centre") final int centre,
            @UdfParameter(value="armWidth") final int armWidth) {

        return membershipTriangle((double) v, centre, armWidth);

    }
    
    @Udf(description = "Find membership for a BIGINT value by fuzzifying with a trapeze.")
    public double membershipTriangle(@UdfParameter(value="v") final long v,
            @UdfParameter(value="centre") final long centre,
            @UdfParameter(value="armWidth") final long armWidth) {

        return membershipTriangle((double) v, centre, armWidth);

    }
}
