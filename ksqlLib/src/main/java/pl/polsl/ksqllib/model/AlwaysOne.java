/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "alwaysOne", description = "use for joins to join all records.")
public class AlwaysOne {
    
    @Udf(description = "Retruns 1 for DOUBLE")
    public int alwaysOne(@UdfParameter(value="v") final double v){
        return 1;
    }
    
    @Udf(description = "Retruns 1 for BIGINT")
    public int alwaysOne(@UdfParameter(value="v") final long v){
        return 1;
    }
    
    @Udf(description = "Retruns 1 for INT")
    public int alwaysOne(@UdfParameter(value="v") final int v){
        return 1;
    }
}
