/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import pl.polsl.ksqllib.linguisticVariable.LinguisticVariable;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "linguistic", description = "Returns a linguistic representation of a value.")
public class Linguistic {

   
    private String readJsonFromFile(String inputPath) throws IOException {

        StringBuilder contentBuilder = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(inputPath))) 
        {

            String sCurrentLine;
            while ((sCurrentLine = br.readLine()) != null) 
            {
                contentBuilder.append(sCurrentLine).append("\n");
            }
        } 
        catch (IOException e) 
        {
            return "ERROR READING THE JSON FILE";
        }
        return contentBuilder.toString();
        
        
    }

    private List<LinguisticVariable> readJson(String json, String attributeName) throws JsonProcessingException {

        List<LinguisticVariable> variables = new ArrayList();

        ObjectMapper objectMapper = new ObjectMapper();
        

        JsonNode jsonNode = objectMapper.readValue(json, JsonNode.class);
        JsonNode attributeNode = jsonNode.get(attributeName);
  
        
        Iterator<String> nodes = attributeNode.fieldNames();
        
        while(nodes.hasNext()){
            
            String nodeName = nodes.next();
            nodeName = nodeName.split(" ")[0];
            
            JsonNode type = attributeNode.get(nodeName);
            LinguisticVariable lingVar = new LinguisticVariable(nodeName, type);
            variables.add(lingVar);
        }

        return variables;
    }

    @Udf(description = "Find linguistic variable for DOUBLE.")
    public String linguistic(@UdfParameter(value = "v") final double v, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) {

        
        try{
            List<LinguisticVariable> vars;

            if (json.endsWith(".json")) {
                vars = readJson(readJsonFromFile(json), attribute);
            } else {
                vars = readJson(json, attribute);
            }


            LinguisticVariable bestVar = vars.get(0);
            double maxLambda = bestVar.getLambdaForValue(v);

            for(int i = 1; i < vars.size(); i++){

                LinguisticVariable var = vars.get(i);
                double lambda = var.getLambdaForValue(v);

                if(lambda > maxLambda){
                    bestVar = var;
                    maxLambda = lambda;
                }
            }

            return bestVar.getName();
        } catch (IOException ex){
            return ex.toString();
        }
    }

    @Udf(description = "Find linguistic variable for INT.")
    public String linguistic(@UdfParameter(value = "v") final int v, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) {
        return linguistic((double) v, attribute, json);
    }

    @Udf(description = "Find linguistic variable for BIGINT.")
    public String linguistic(@UdfParameter(value = "v") final long v, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) {
        return linguistic((double) v, attribute, json);
    }
    
    @Udf(description = "Find affiliation of a DOUBLE in relation to given linguistic variable.")
    public double linguistic(@UdfParameter(value = "v") final double v, @UdfParameter(value = "lingVar") final String lingVar, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) throws IOException {
        
        List<LinguisticVariable> vars;

        if (json.endsWith(".json")) {
            vars = readJson(readJsonFromFile(json), attribute);
        } else {
            vars = readJson(json, attribute);
        }


        LinguisticVariable chosenVar = vars.get(0);

        for(int i = 1; i < vars.size(); i++){

            LinguisticVariable var = vars.get(i);


            if(var.getName().compareTo(lingVar) == 0){
                chosenVar = var;
                break;
            }
        }

        return chosenVar.getLambdaForValue(v);

    }
    
    @Udf(description = "Find affiliation of a BIGINT in relation to given linguistic variable.")
    public double linguistic(@UdfParameter(value = "v") final long v, @UdfParameter(value = "lingVar") final String lingVar, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) throws IOException {
        
        return linguistic((double) v, lingVar, attribute, json);
    }
    
    @Udf(description = "Find affiliation of a INT in relation to given linguistic variable.")
    public double linguistic(@UdfParameter(value = "v") final int v, @UdfParameter(value = "lingVar") final String lingVar, @UdfParameter(value = "attribute") final String attribute, @UdfParameter(value = "json") final String json) throws IOException {
        
        return linguistic((double) v, lingVar, attribute, json);
    }

}
