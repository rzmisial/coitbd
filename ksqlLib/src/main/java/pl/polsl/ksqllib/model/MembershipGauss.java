/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueGauss;
import pl.polsl.ksqllib.fuzzyValue.IFuzzyValue;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "membershipGauss", description = "Fuzzifies a value and returns its membership to the fuzzifying Gaussian curve.")
public class MembershipGauss {
    
    @Udf(description = "Find affiliation for a DOUBLE value by fuzzifying with a Gaussuan curve.")
    public double membershipGauss(@UdfParameter(value="v") final double v,
            @UdfParameter(value="a") final double m,
            @UdfParameter(value="std") final double std) {

        IFuzzyValue val = new FuzzyValueGauss(m, std);
        return val.getLambda(v);
    }
    
    @Udf(description = "Find membership for a INT value by fuzzifying with a Gaussuan curve.")
    public double membershipGauss(@UdfParameter(value="v") final int v,
            @UdfParameter(value="m") final double m,
            @UdfParameter(value="std") final double std) {

        return membershipGauss((double) v, m, std);

    }
    
    @Udf(description = "Find membership for a BIGINT value by fuzzifying with a Gaussuan curve.")
    public double membershipGauss(@UdfParameter(value="v") final long v,
            @UdfParameter(value="m") final double m,
            @UdfParameter(value="std") final double std) {

        return membershipGauss((double) v, m, std);

    }
}
