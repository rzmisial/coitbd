/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "fuzzNot", description = "Negate affiliation results.")
public class FuzzNot {
    
    @Udf(description = "Negate affiliation results.")
    public double fuzzNot(double a) throws Exception{
        
        if(a > 1.0 || a < 0.0)
            throw new Exception("Membership must be in range <0, 1>. Is " + a + ".");
        
        return 1.0 - a;
    }
}
