/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;

/**
 *
 * @author Michal
 */

@UdfDescription(name = "getDir", description = "Returns the current workig directory.")
public class GetDir {
    
    @Udf(description = "Returns the current workig directory.")
    public String getDir(){
        
        return System.getProperty("user.dir");
    }
    
}
