/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "fuzzOr", description = "Join affiliation results with an s-norm.")
public class FuzzOr {
   
    @Udf(description = "Join affiliation results with an s-norm.")
    public double fuzzOr(@UdfParameter(value="a") final double ... a/*, @UdfParameter(value="b") final double b*/){
        
        double result = 0;
        for(double val : a){
            if(val > result)
                result = val;
        }
        return result;
        
    }
}
