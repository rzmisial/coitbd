/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "fuzzAnd", description = "Join affiliation results with an t-norm.")
public class FuzzAnd {
    
    @Udf(description = "Join affiliation results with an s-norm.")
    public double fuzzAnd(@UdfParameter(value="a") final double ... a/*, @UdfParameter(value="b") final double b*/){
        
        double result = 1;
        for(double val : a){
            if(val < result)
                result = val;
        }
        return result;
    }
}
