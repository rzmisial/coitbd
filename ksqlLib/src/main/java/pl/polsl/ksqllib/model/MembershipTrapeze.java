/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTrapeze;
import pl.polsl.ksqllib.fuzzyValue.IFuzzyValue;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "membershipTrapeze", description = "Fuzzifies a value and returns its membership to the fuzzifying trapeze.")
public class MembershipTrapeze {

    @Udf(description = "Find membership for a DOUBLE value by fuzzifying with a trapeze.")
    public double membershipTrapeze(@UdfParameter(value="v") final double v,
            @UdfParameter(value="a") final double a,
            @UdfParameter(value="b") final double b,
            @UdfParameter(value="c") final double c,
            @UdfParameter(value="d") final double d) {

        IFuzzyValue val = new FuzzyValueTrapeze(a, b, c, d);
        return val.getLambda(v);
    }
    
    @Udf(description = "Find membership for a INT value by fuzzifying with a trapeze.")
    public double membershipTrapeze(@UdfParameter(value="v") final int v,
            @UdfParameter(value="a") final double a,
            @UdfParameter(value="b") final double b,
            @UdfParameter(value="c") final double c,
            @UdfParameter(value="d") final double d) {

        return membershipTrapeze((double) v, a, b, c, d);

    }
    
    @Udf(description = "Find membership for a BIGINT value by fuzzifying with a trapeze.")
    public double membershipTrapeze(@UdfParameter(value="v") final long v,
            @UdfParameter(value="a") final double a,
            @UdfParameter(value="b") final double b,
            @UdfParameter(value="c") final double c,
            @UdfParameter(value="d") final double d) {

        return membershipTrapeze((double) v, a, b, c, d);

    }
    
    @Udf(description = "Find membership for a DOUBLE value by fuzzifying with a trapeze.")
    public double membershipTrapeze(@UdfParameter(value="v") final double v,
            @UdfParameter(value="centre") final double centre,
            @UdfParameter(value="baseWidth") final double baseWidth,
            @UdfParameter(value="armWidth") final double armWidth) {

        IFuzzyValue val = new FuzzyValueTrapeze(centre, baseWidth, armWidth);
        return val.getLambda(v);
    }
    
    @Udf(description = "Find membership for a INT value by fuzzifying with a trapeze.")
    public double membershipTrapeze(@UdfParameter(value="v") final int v,
            @UdfParameter(value="centre") final double centre,
            @UdfParameter(value="baseWidth") final double baseWidth,
            @UdfParameter(value="armWidth") final double armWidth) {

        return membershipTrapeze((double) v, centre, baseWidth, armWidth);

    }
    
    @Udf(description = "Find membership for a BIGINT value by fuzzifying with a trapeze.")
    public double affiliationTrapeze(@UdfParameter(value="v") final long v,
            @UdfParameter(value="centre") final double centre,
            @UdfParameter(value="baseWidth") final double baseWidth,
            @UdfParameter(value="armWidth") final double armWidth) {

        return membershipTrapeze((double) v, centre, baseWidth, armWidth);

    }
}
