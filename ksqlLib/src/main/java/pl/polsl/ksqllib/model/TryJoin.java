/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.model;

import io.confluent.ksql.function.udf.Udf;
import io.confluent.ksql.function.udf.UdfDescription;
import io.confluent.ksql.function.udf.UdfParameter;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueGauss;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTrapeze;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTriangle;
import pl.polsl.ksqllib.fuzzyValue.IFuzzyValue;

/**
 *
 * @author Michal
 */
@UdfDescription(name = "tryJoin", description = "Check if two fuzzy values can be joined at given lambda.")
public class TryJoin {
    
    IFuzzyValue getFuzzyValue(String [] params, double val){
        
        String type = params[0];
        type = type.toUpperCase();
        
        Double baseWidth;
        Double armWidth;
        
        switch(type){
            
            case "TRAPEZE":
                baseWidth = Double.valueOf(params[1]);
                armWidth = Double.valueOf(params[2]);
                return new FuzzyValueTrapeze(val, baseWidth, armWidth);
                
            case "TRIANGLE":
                armWidth = Double.valueOf(params[1]);
                return new FuzzyValueTriangle(val, armWidth);
                
            case "GAUSS":
                Double mean = Double.valueOf(params[1]);
                Double stdDev = Double.valueOf(params[2]);
                return new FuzzyValueGauss(mean, stdDev);
            default:
                return null;
            
        }
    }
    
    @Udf(description = "Check if two fuzzy values can be joined at given lambda.")
    public boolean tryJoin(@UdfParameter(value="v1") final double v1,
                           @UdfParameter(value="fuzzyDef1") final String fuzzyDef1,
                           @UdfParameter(value="v2") final double v2,
                           @UdfParameter(value="fuzzyDef2") final String fuzzyDef2,
                           @UdfParameter(value="lambda") final double lambda) throws Exception{
        
        
        String [] params1 = fuzzyDef1.split(",");
        String [] params2 = fuzzyDef2.split(",");
        
        for(int i = 0; i < params1.length; i++)
            params1[i] = params1[i].replace(" ", "");
        for(int i = 0; i < params1.length; i++)
            params2[i] = params2[i].replace(" ", "");
        
        IFuzzyValue fuzzyVal1 = getFuzzyValue(params1, v1);
        IFuzzyValue fuzzyVal2 = getFuzzyValue(params1, v2);
        
        
        double[] range1 = fuzzyVal1.findRangeForLambda(lambda);
        double[] range2 = fuzzyVal2.findRangeForLambda(lambda);
        
        if(range1[0] < range2[0]){
            return range2[0] < range1[1];
        } else {
            return range1[0] < range2[1];
        }
    }
    
    @Udf(description = "Check if two fuzzy values can be joined at given lambda.")
    public boolean tryJoin(@UdfParameter(value="v1") final long v1,
                           @UdfParameter(value="fuzzyDef1") final String fuzzyDef1,
                           @UdfParameter(value="v2") final long v2,
                           @UdfParameter(value="fuzzyDef2") final String fuzzyDef2,
                           @UdfParameter(value="lambda") final double lambda) throws Exception{
        
        return tryJoin((double) v1, fuzzyDef1, (double) v2, fuzzyDef2, lambda);
    }
    
    @Udf(description = "Check if two fuzzy values can be joined at given lambda.")
    public boolean tryJoin(@UdfParameter(value="v1") final int v1,
                           @UdfParameter(value="fuzzyDef1") final String fuzzyDef1,
                           @UdfParameter(value="v2") final int v2,
                           @UdfParameter(value="fuzzyDef2") final String fuzzyDef2,
                           @UdfParameter(value="lambda") final double lambda) throws Exception{
        
        return tryJoin((double) v1, fuzzyDef1, (double) v2, fuzzyDef2, lambda);
    }
    
}
