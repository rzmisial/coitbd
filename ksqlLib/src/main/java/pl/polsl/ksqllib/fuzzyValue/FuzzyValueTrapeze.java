/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.fuzzyValue;

import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author Michal
 */
public class FuzzyValueTrapeze implements IFuzzyValue {
    
    protected double a;
    protected double b;
    protected double c;
    protected double d;
    
    protected double centre;
    
    
    
    public FuzzyValueTrapeze(double a, double b, double c, double d){
        
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        
        completeConstruction();
    }
    
    public FuzzyValueTrapeze(double centre, double baseWidth, double armWidth){
        
        this.centre = centre;
        this.b = centre - baseWidth/2;
        this.c = centre + baseWidth/2;
        
        this.a = b - armWidth;
        this.d = c + armWidth;
        
        completeConstruction();
    }
    
    public FuzzyValueTrapeze(JsonNode params){
        
        this.a = params.get("a").asDouble();
        this.b = params.get("b").asDouble();
        this.c = params.get("c").asDouble();
        this.d = params.get("d").asDouble();
        
        completeConstruction();
    }
    
    protected void completeConstruction(){
        
        centre = (c+b)/2;
    }
    
    
    
    @Override
    public double getLambda(double value){
        
        if(value < a || value > d)
            return 0.0;
        else if(value >= a && value < b){
            
            double range = b - a;
            double valueAffiliation = value - a;
            
            return valueAffiliation / range;
            
        } else if (value > c && value <= d){
            
            double range = d - c;
            double valueAffiliation = d - value;
            
            return valueAffiliation / range;
        } else
            return 1.0;
    }

    /*
    public double findIntersect(IFuzzyValue val) {
        
        if(val instanceof FuzzyValueTrapeze){
            
            FuzzyValueTrapeze v = (FuzzyValueTrapeze) val;
            
            if(b >= v.b && b <= v.c || v.b >= b && v.b <= c){
                return 1.0;
            } else if (d < v.a || v.d < a){
                return 0.0;
            } else {
                
                double Y = 0.0;
                double maxIntersect = 0.0;
                
                Y = findSegmentIntersect(Y, a, 0, b, 1, v.a, 0, v.b, 1);
                if(Y >= 0){
                    if(Y > maxIntersect)
                        maxIntersect = Y;
                }
                
                Y = findSegmentIntersect(Y, a, 0, b, 1, v.c, 1, v.d, 0);
                if(Y >= 0){
                    if(Y > maxIntersect)
                        maxIntersect = Y;
                }
                
                Y = findSegmentIntersect(Y, c, 1, d, 0, v.a, 0, v.b, 1);
                if(Y >= 0){
                    if(Y > maxIntersect)
                        maxIntersect = Y;
                }
                
                Y = findSegmentIntersect(Y, c, 1, d, 0, v.c, 1, v.d, 0);
                if(Y >= 0){
                    if(Y > maxIntersect)
                        maxIntersect = Y;
                }
                
                return maxIntersect;
            }
            
        }
        
        return 0.0;
    }
    
    private double findSegmentIntersect(double Y,
            double x11, double y11,
            double x12, double y12,
            double x21, double y21,
            double x22, double y22
            ){
        
        if(x11 == x12 && x21 == x22){
            return -1.0;
        }
        if(x11 == x12){
            //TODO
        }
        if(x21 == x22){
            //TODO
        }
        
        double m1 = (y12-y11) / (x12-x11); 
        double b1 = y11 - m1*x11; 
        double m2 = (y22-y21) / (x22-x21);
        double b2 = y21 - m2*x21;

        if (m1 == m2) {
            return -1.0;
        }

        double X = (b2 - b1) / (m1 - m2);
        Y = m1 * X + b1;
        
        if(X < x11 || X > x12 || X < x21 || X > x22)
            return -1.0;
        
        
        return Y;
        
    }
*/

    @Override
    public double[] findRangeForLambda(double lambda) throws Exception {
        
        if(lambda < 0.0 || lambda > 1.0){
            throw new Exception("Lambda must be in range of <0, 1>");
        }
        
        double start = a + (b - a) * lambda;
        double end = d - (d - c) * lambda;
        
        double [] range = new double[2];
        range[0] = start;
        range[1] = end;
        
        return range;
    }
}
