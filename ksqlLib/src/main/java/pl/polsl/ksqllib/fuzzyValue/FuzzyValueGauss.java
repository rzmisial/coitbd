/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.fuzzyValue;

import com.fasterxml.jackson.databind.JsonNode;
import org.apache.commons.math3.analysis.function.Gaussian;

/**
 *
 * @author Michal
 */
public class FuzzyValueGauss implements IFuzzyValue {

    private double mean;
    private double std;
    private Gaussian gaussian;
    private double rawValueMean;
    
    public FuzzyValueGauss(double mean, double std){
        
        this.mean = mean;
        this.std = std;
        
        this.gaussian = new Gaussian(mean, std);
        this.rawValueMean = gaussian.value(mean);
    }
    
    public FuzzyValueGauss(JsonNode params){
        
        this.mean = params.get("m").asDouble();
        this.std = params.get("std").asDouble();
        
        this.gaussian = new Gaussian(mean, std);
        this.rawValueMean = gaussian.value(mean);
        
    }
    
    @Override
    public double getLambda(double value) {
        
        double lambda = gaussian.value(value);
        
        return lambda / rawValueMean;
    }

    @Override
    public double[] findRangeForLambda(double lambda) throws Exception {
        
        double correctedLambda = lambda / rawValueMean;
        double searchDelta = std / 10;
        
        double start;
        double end;
        double foundValue;
        
        long count = 0;
        do {
            searchDelta *= 10;
            start = mean - searchDelta;
            foundValue = gaussian.value(start);
            count++;
        } while(foundValue > correctedLambda);
        
        do {
            searchDelta /= 2;
            if(foundValue > correctedLambda){
                
                start = start - searchDelta;
            }
            else{
                start = start + searchDelta;
            }
            
            foundValue = gaussian.value(start);
            count++;
        } while(foundValue < correctedLambda - 0.000001 || foundValue > correctedLambda + 0.000001);
        
        System.out.println(count);
        end = mean + (mean - start);
        
        double [] range = new double[2];
        range[0] = start;
        range[1] = end;
        
        return range;
        
    }

    
}
