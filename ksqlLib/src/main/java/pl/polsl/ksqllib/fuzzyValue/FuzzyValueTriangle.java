/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.fuzzyValue;

import com.fasterxml.jackson.databind.JsonNode;

/**
 *
 * @author Michal
 */
public class FuzzyValueTriangle extends FuzzyValueTrapeze {
    
    public FuzzyValueTriangle(double a, double b, double c){
        
        super(a, b, b, c);
    }
    
    public FuzzyValueTriangle(double centre, double armWidth){
        
        super(centre, 0, armWidth);
    }
    
    public FuzzyValueTriangle(JsonNode params){
        
        super(  params.get("a").asDouble(),
                params.get("b").asDouble(),
                params.get("b").asDouble(),
                params.get("c").asDouble());
    }
}
