/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.fuzzyValue;

/**
 *
 * @author Michal
 */
public interface IFuzzyValue {

    
    /**
     * Calculates the lambda for the given value.
     * 1 represents perfect correspondence. 0 represents no correspondence.
     * 
     * @param value Value to calculate lambda for,
     * @return Calculated lambda (ranged from 0 to 1),
     */
    public double getLambda(double value);
    
    /**
     * Calculates the range for which the membership is higher or equal lambda.
     * 
     * @param value Value to calculate lambda for,
     * @return Calculated lambda (ranged from 0 to 1),
     */
    public double [] findRangeForLambda(double lambda) throws Exception;
    
    /*
    **
     * Calculates the intersect point for this fuzzy value and the fuzzy value provided as parameter.
     * 
     * @param val The other fuzzy value to find intersect with.
     * @return The intersect of 2 fuzzy values.
     *
    public double findIntersect(IFuzzyValue val);
    */
}
