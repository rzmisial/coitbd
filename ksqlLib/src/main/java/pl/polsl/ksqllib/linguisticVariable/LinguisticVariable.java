/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.polsl.ksqllib.linguisticVariable;

import com.fasterxml.jackson.databind.JsonNode;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueGauss;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTrapeze;
import pl.polsl.ksqllib.fuzzyValue.FuzzyValueTriangle;
import pl.polsl.ksqllib.fuzzyValue.IFuzzyValue;

/**
 *
 * @author Michal
 */
public class LinguisticVariable {
    
    private String name;
    private IFuzzyValue fuzzyRange;
    
    public LinguisticVariable(String name, IFuzzyValue fuzzyRange){
        this.name = name;
        this.fuzzyRange = fuzzyRange;
    }
    
    public LinguisticVariable(String name, JsonNode params){
        this.name = name;
        
        String type = params.get("type").asText();
        
        switch(type){
            case "trapeze":
                this.fuzzyRange = new FuzzyValueTrapeze(params);
                break;
            
            case "triangle":
                this.fuzzyRange = new FuzzyValueTriangle(params);
                break;
            case "gauss":
                this.fuzzyRange = new FuzzyValueGauss(params);
                break;
        }
    }
    
    /**
     * Assigns a lambda for the given value using the fuzzy value
     * this linguistic value represents.
     * 
     * @param value
     * @return 
     */
    public double getLambdaForValue(double value){
        
       return fuzzyRange.getLambda(value);
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the fuzzyRange
     */
    public IFuzzyValue getFuzzyRange() {
        return fuzzyRange;
    }

    /**
     * @param fuzzyRange the fuzzyRange to set
     */
    public void setFuzzyRange(IFuzzyValue fuzzyRange) {
        this.fuzzyRange = fuzzyRange;
    }
}
